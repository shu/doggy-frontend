# # stage1 as builder
FROM node:14 as builder

RUN npm install -g pm2

#移动当前⽬目录下⾯面的⽂文件到app⽬目录下
ADD . /ssr-front/
#进⼊入到app⽬目录下⾯面，类似cd
WORKDIR /ssr-front
# Copy the package.json and install dependencies
COPY package*.json ./
RUN npm install
RUN npm run build
# Copy rest of the files
COPY . .
EXPOSE 3000

CMD [ "pm2-runtime", "start", "ecosystem.config.js"]
