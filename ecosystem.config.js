module.exports = {
  apps: [{
    name: 'ssr-front',
    script: 'dist/ssr/index.js',
    // 分為 cluster 以及 fork 模式
    exec_mode: 'cluster',
    // 只適用於 cluster 模式，程序啟動數量
    instances: 2

  }],

  deploy: {
    production: {
      user: 'SSH_USERNAME',
      host: 'SSH_HOSTMACHINE',
      ref: 'origin/master',
      repo: 'GIT_REPOSITORY',
      path: 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
}
