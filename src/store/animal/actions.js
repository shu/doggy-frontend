
import { apiGetAnimalList, apiGetAnimal } from '@api/animal'

export async function getAnimalList ({ commit }, postData) {
  try {
    const resData = await this.$axios({ ...apiGetAnimalList(), data: postData })

    commit('setAnimalList', resData.data)
    return resData.data
  } catch (error) {
    return error
  }
}
export async function getAnimal ({ commit }, postData) {
  try {
    const resData = await this.$axios({ ...apiGetAnimal(), params: postData })
    commit('setAnimal', resData.data)
    return resData.data
  } catch (error) {
    return error
  }
}
