
// export default function () {
//   const user = JSON.parse(localStorage.getItem('user'))
//   const initialState = user
//     ? { loggedIn: true, user }
//     : { loggedIn: false, user: null }
//   return initialState
// }
export default function () {
  return {
    isLogged: false,
    user: null
  }
}
