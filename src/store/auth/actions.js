
import { apiLogin } from '@api/auth'
import { setCookies } from '@utils/cookies'
// export function login ({ commit }, user) {
//   return AuthService.login(user).then(
//     user => {
//       commit('loginSuccess', user)
//       return Promise.resolve(user)
//     },
//     error => {
//       commit('loginFailure')
//       return Promise.reject(error)
//     }
//   )
// }
export async function login ({ commit }, postData) {
  try {
    const resData = await this.$axios({ ...apiLogin(), data: postData })

    setCookies('token', resData.data.token)
    setCookies('user', resData.data.user)
    commit('loginSuccess', resData.data)
    return resData.data
  } catch (error) {
    return error
  }
}
// export function logout ({ commit }) {
//   AuthService.logout()
//   commit('logout')
// }
