export function apiLogin () {
  return {
    url: 'api/auth/login',
    method: 'post'
  }
}
